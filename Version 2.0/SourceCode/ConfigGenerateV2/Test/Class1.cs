﻿//
//    Copyright 2016 KeyleXiao.
//    Contact to Me : Keyle_xiao@hotmail.com 
//
//   	Licensed under the Apache License, Version 2.0 (the "License");
//   	you may not use this file except in compliance with the License.
//   	You may obtain a copy of the License at
//
//   		http://www.apache.org/licenses/LICENSE-2.0
//
//   		Unless required by applicable law or agreed to in writing, software
//   		distributed under the License is distributed on an "AS IS" BASIS,
//   		WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   		See the License for the specific language governing permissions and
//   		limitations under the License.
//
using IGeneratorContract;
using System.Collections.Generic;

namespace Test
{
    public class Class1 : IPluginContract
    {
        public string PluginName
        {
            get
            {
                return "测试";
            }
        }

        public string WarningConfirmText
        {
            get
            {
                return "你确认已经加载过文件吗？";
            }
        }

        public string WorkBookName { get; set; }
        public IContext Context { get; set; }

        public string PluginAuthorInfo
        {
            get
            {
                return "插件作者信息";
            }
        }

        public bool ExecutedStatus { get; set; }
        public string ExecutedMsg { get; set; }
    }
}
