﻿//
//    Copyright 2016 KeyleXiao.
//    Contact to Me : Keyle_xiao@hotmail.com 
//
//   	Licensed under the Apache License, Version 2.0 (the "License");
//   	you may not use this file except in compliance with the License.
//   	You may obtain a copy of the License at
//
//   		http://www.apache.org/licenses/LICENSE-2.0
//
//   		Unless required by applicable law or agreed to in writing, software
//   		distributed under the License is distributed on an "AS IS" BASIS,
//   		WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   		See the License for the specific language governing permissions and
//   		limitations under the License.
//
using System;
using System.Collections.Generic;
using IGeneratorContract;
using System.Configuration;
using System.Reflection;
using System.IO;

namespace ConfigGeneratorV2UI.Config
{
    public class EditorContext: IContext
    {
        private EditorContext()
        {
            //Init here
            if (ExcelInfos == null)
                ExcelInfos = new Dictionary<IExcelModel, bool>();

            AUTHOR_INFO = ConfigurationSettings.AppSettings["AUTHOR_INFO"];
            OutputPath = ConfigurationSettings.AppSettings["DefaultExportDirectory"];
            if (string.IsNullOrEmpty(OutputPath))
            {
                OutputPath =Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }
        }

        private static EditorContext instance;
        public static EditorContext Instance
        {
            get { return instance ?? (instance = new EditorContext()); }
        }

        public Dictionary<IExcelModel, bool> ExcelInfos { get; set; }
        //public IExcelModel ExcelInfo { get; set; } 
        public string OutputPath { get; set; }

        public string AUTHOR_INFO { get; set; }

        public IExcelModel SearchExcelInfosWithSheetName(string sheetName)
        {
            foreach (var item in ExcelInfos.Keys)
            {
                if (item.SheetName == sheetName)
                {
                    return item;
                }
            }
            return null;
        }
    }
}
